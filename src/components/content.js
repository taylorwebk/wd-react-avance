export default ([
  {
    id: 0,
    text: 'Why React is a Must Today?',
    subItems: [
      {
        id: 0,
        text: 'History and Growth.'
      }, {
        id: 1,
        text: 'React in 2019.'
      }, {
        id: 2,
        text: 'React advanteges.'
      }, {
        id: 3,
        text: 'Components... components everywhere.'
      }, {
        id: 4,
        text: 'What do I need to know to begin?.'
      }
    ]
  }, {
    id: 1,
    text: 'ES6+',
    subItems: [
      {
        id: 0,
        text: 'What is ES6, ES7, ES8...?'
      }, {
        id: 1,
        text: 'let and const'
      }, {
        id: 2,
        text: 'Template Literals'
      }, {
        id: 3,
        text: 'Destructuring'
      }, {
        id: 4,
        text: 'ES6 Modules'
      }, {
        id: 5,
        text: 'Default Parameters'
      }, {
        id: 6,
        text: 'Spread Operator'
      }, {
        id: 7,
        text: 'Arrow Functions'
      }, {
        id: 8,
        text: 'ArrayPrototype.forEach().map().filter()'
      }, {
        id: 9,
        text: 'Promises'
      }, {
        id: 10,
        text: 'Async...Await'
      }
    ]
  }, {
    id: 2,
    text: 'Environment Setup (Webpack 4)',
    subItems: [
      {
        id: 0,
        text: 'Webpack vs. other react starter solutions (Parcel JS, create-react-app CLI, third-party repos).'
      }, {
        id: 1,
        text: 'Enable loaders (css, html, files, fonts, images).'
      }, {
        id: 2,
        text: 'Hello World.'
      }, {
        id: 3,
        text: 'Hot loading.'
      }
    ]
  }, {
    id: 3,
    text: 'React Basics',
    subItems: [
      {
        id: 0,
        text: 'How React works.'
      }, {
        id: 1,
        text: 'React props.'
      }, {
        id: 2,
        text: 'React state.'
      }, {
        id: 3,
        text: 'JSX.'
      }, {
        id: 4,
        text: 'Function components.'
      }, {
        id: 5,
        text: 'Class components.'
      }, {
        id: 6,
        text: 'React lifecycle methods.'
      }
    ]
  }, {
    id: 4,
    text: 'Writing Better Code with ESLint',
    subItems: [
      {
        id: 0,
        text: 'What is ESLint?'
      }, {
        id: 1,
        text: 'Setup and running ESLint'
      }
    ]
  }, {
    id: 5,
    text: 'APPLICATION CASE I: TO DO LIST',
    subItems: [
      {
        id: 0,
        text: 'Really...?? Another TO DO LIST App...??'
      }, {
        id: 1,
        text: 'Preparing app for Production.'
      }
    ]
  }
])
