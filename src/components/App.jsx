/* eslint-disable react/jsx-one-expression-per-line */
import React, { useState, useEffect } from 'react'
import { hot } from 'react-hot-loader/root'
import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'

import NestedList from './NestedList'
import withTheme from './withTheme'

import logoSvg from './logo.svg'

import './styles.css'

const key = '6c6aaf8407122dab109e9fb827251183'
const formId = '91043474997670'

const Header = ({ src }) => (
  <div className="App-header">
    <img src={src} className="App-logo" alt="" />
    <Typography variant="h3"> What will we learn? </Typography>
    <Typography variant="h5" color="primary"> Pre-requisites: </Typography>
    <Typography variant="body2" color="textSecondary"> - Good Undestanding of JS. </Typography>
    <Typography variant="body2" color="textSecondary"> - Laptop with al least 4Gb of RAM. </Typography>
    <Typography variant="body2" color="textSecondary"> - Internet (Mobile Data), at least 150Mb per class. </Typography>
  </div>
)

const App = () => {
  const [nro, setNro] = useState(0)
  const [enabled, setEnabled] = useState(false)
  const [lastSub, setLastSub] = useState('')

  useEffect(() => {
    fetch(`https://api.jotform.com/form/${formId}?apiKey=${key}`)
      .then(res => res.json())
      .then((res) => {
        console.log(res)
        setNro(res.content.count)
        setLastSub(res.content.last_submission)
        setEnabled(res.content.status === 'ENABLED')
      })
  }, [])
  return (
    <div className="App">
      <Header src={logoSvg} nro={nro} />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <NestedList />
      <br />
      <br />
      <Typography variant="h5" color="primary">{`Currently enrolled: ${nro}`}</Typography>
      <Typography variant="h5" color={enabled ? 'primary' : 'error'}>{`Available: ${99 - nro}`}</Typography>
      {
        lastSub && (
          <Typography variant="body2">{`Last Submission: ${lastSub}`}</Typography>
        )
      }
      <br />
      {
        nro === 99
          && (
          <Typography variant="h5" color="error">
            Sadly there is no space left in the course, you can come to the courses but you will not have an attendance certificate.
          </Typography>
          )
      }
      <Button
        disabled={!enabled}
        style={{ maxWidth: 660, width: '100%' }}
        color="primary"
        variant="contained"
        onClick={() => {
          window.open('https://form.jotformz.com/91043474997670', '_blank')
        }}
      >
        {
          enabled ? 'I want take the Course' : 'The form is unvailable for now.'
        }
      </Button>
      <br />
      <br />
    </div>
  )
}

export default hot(withTheme(App))
