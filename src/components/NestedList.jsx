import React, { Fragment } from 'react'
import { withStyles } from '@material-ui/core/styles'
import ListSubheader from '@material-ui/core/ListSubheader'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import Collapse from '@material-ui/core/Collapse'
import Paper from '@material-ui/core/Paper'

import ExpandLess from '@material-ui/icons/ExpandLess'
import ExpandMore from '@material-ui/icons/ExpandMore'
import TrendingFlatIcon from '@material-ui/icons/TrendingFlat'

import content from './content'


const styles = theme => ({
  root: {
    width: '100%',
    maxWidth: 660,
    backgroundColor: theme.palette.background.paper
  },
  nested: {
    paddingLeft: theme.spacing.unit * 12
  }
})

class NestedList extends React.Component {
  state = {
    openItems: []
  };

  handleClick = id => () => {
    const { openItems } = this.state
    if (openItems.indexOf(id) !== -1) {
      this.setState(state => ({
        openItems: state.openItems.filter(item => item !== id)
      }))
    } else {
      this.setState(state => ({ openItems: [...state.openItems, id] }))
    }
  };

  render() {
    const { classes } = this.props
    const { openItems } = this.state

    return (
      <Paper elevation={10} className={classes.root}>
        <List
          component="nav"
          subheader={<ListSubheader component="div">MODULE I: Starts April 27</ListSubheader>}
        >
          {
            content.map(item => (
              <Fragment key={item.id}>
                <ListItem button onClick={this.handleClick(item.id)}>
                  <ListItemText color="#f00" inset primary={`${item.id}. ${item.text}`} />
                  {openItems.indexOf(item.id) >= 0 ? <ExpandLess /> : <ExpandMore />}
                </ListItem>
                <Collapse in={openItems.indexOf(item.id) >= 0} timeout="auto" unmountOnExit>
                  <List component="div" disablePadding>
                    {
                      item.subItems.map(subItem => (
                        <ListItem key={subItem.id} className={classes.nested}>
                          <ListItemIcon>
                            <TrendingFlatIcon />
                          </ListItemIcon>
                          <ListItemText inset primary={subItem.text} />
                        </ListItem>
                      ))
                    }
                  </List>
                </Collapse>
              </Fragment>
            ))
          }
        </List>
      </Paper>
    )
  }
}

export default withStyles(styles)(NestedList)
